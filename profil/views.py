from django.shortcuts import render
from .models import Company

# Create your views here.
def index(request):
	response = {'author': 'Azahra Putri Andani'}
	html = 'profil/profil.html'
	company = Company(nama='Kelompok 2 PPW A', foto="https://i.imgur.com/Y0YQB3u.png", tipe="Institusi Pendidikan", jumlah_pegawai="2-10 orang", web="http://ppw-tp2-a.herokuapp.com", spesialitas="JavaScript, Python, HTML, CSS", lokasi="Depok, Jawa Barat")
	company.save()
	companies = Company.objects.all()
	response['companies'] = companies
	response['firstCompany'] = Company.objects.all().last()
	return render(request, html, response)