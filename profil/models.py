from django.db import models

# Create your models here.
class Company(models.Model):
	nama = models.CharField(max_length=100, default="Kelompok 2 PPW A")
	foto = models.CharField(max_length=500)
	tipe = models.CharField(max_length=35)
	jumlah_pegawai = models.CharField(max_length=20)
	web = models.URLField()
	spesialitas = models.CharField(max_length=200)
	lokasi = models.CharField(max_length=50)

