from django.conf.urls import url
from .views import index, add_comment, delete_model, add_forum

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'add_comment/$', add_comment, name='add_comment'),
	url(r'add_forum/$', add_forum, name='add_forum'),
	url(r'delete_model/$', delete_model, name='delete_model'),
]
