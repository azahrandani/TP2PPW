from django.db import models
from django.utils import timezone

# Create your models here.
class Forum(models.Model):
    user_id = models.CharField(max_length=200)
    title = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    body = models.TextField()
    pp = models.CharField(max_length=200, default="https://2.bp.blogspot.com/-kgFUS3eumWU/V3JTqLZfvPI/AAAAAAAAA8I/vX3SyELWmn8CrPO9DDW6pw8bttlDdJ86ACLcB/s1600/nobody_m.original.jpg")
    created_date = models.DateTimeField(default=timezone.now)
    
class Comment(models.Model):
    user_id = models.CharField(max_length=200)
    post = models.ForeignKey(Forum, related_name='comments')
    name = models.CharField(max_length=200)
    body = models.TextField()
    pp = models.CharField(max_length=200, default="https://2.bp.blogspot.com/-kgFUS3eumWU/V3JTqLZfvPI/AAAAAAAAA8I/vX3SyELWmn8CrPO9DDW6pw8bttlDdJ86ACLcB/s1600/nobody_m.original.jpg")
    created_date = models.DateTimeField(default=timezone.now)
    class Meta:
        ordering = ['created_date']
